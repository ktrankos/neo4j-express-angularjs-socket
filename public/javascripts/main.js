var watsonApp = angular.module('watson', ['ngRoute', 'matchmedia-ng', 'ngAnimate']);

watsonApp.controller('mainCtrl', function ($scope, $route, $routeParams, $location) 
{	

});
watsonApp.controller('headerCtrl', ['$scope', '$route', function ($scope, $route) 
{
	$scope.location = 'no lo hay';
}]);
watsonApp.controller('chatCtrl', ['$scope', '$route', '$routeParams', function ($scope, $route, $routeParams) 
{	
}]);
watsonApp.controller('trabajosCtrl', function ($scope, $http) 
{

	var socket = io.connect();
	////formulario
	$scope.name = "";
	$scope.information = "";
	$scope.uid = -1;
	$scope.btText = 'Enviar';
	/////
	$scope.works = false;

	socket.on('add work', function(data)
	{		
		$scope.works.push(data);
		$scope.$apply();
	});
	socket.on('clear work', function(data)
	{
		angular.forEach($scope.works, function(value, key) {		
		  if(value._id==data.work)  _key = key;
		});		
		$scope.works.splice(_key, 1);
		$scope.$apply();
	});
	socket.on('change work', function(data)
	{
		angular.forEach($scope.works, function(value, key) {		
		  if(value._id==data._id)
		  {
		  	console.log(data);
		  	value.name = data.name;
		  	value.information = data.information;

		  }
		});
		$scope.$apply();
	});
	///Delete oneby one
	$scope.Delete = function($event, $id){
		console.log("ID", $id);
		var params = { id: $id };
	    var config = {params: params};
		$http.delete('/work/'+$id).
		success(function(data, status, headers, config) 
		{	
			socket.emit('delete work', {work: $id});
		}).
		error(function(data, status, headers, config) {
			//called asynchronously if an error occurs
			//or server returns response with an error status.
		});
	}
	$scope.Reset = function(){
		$scope.name = "";
		$scope.information = "";
		$scope.uid = -1;

		$scope.nuevoTrabajo.$setPristine();
		$scope.nuevoTrabajo.$setUntouched();
		$scope.nuevoTrabajo.name.$setPristine();
		$scope.nuevoTrabajo.name.$setUntouched();
		$scope.nuevoTrabajo.information.$setPristine();
		$scope.nuevoTrabajo.information.$setUntouched();

		$scope.btText = 'Enviar';
	}
	////////////////////////////////EDIT
	$scope.Edit = function($event, $id){
		
		$scope.btText = 'Actualizar';
		$http.get('/work/'+$id).
		success(function(data, status, headers, config) 
		{	
			$scope.name = data.works.name
			$scope.information = data.works.information;
			$scope.uid = data.works._id;
		}).
		error(function(data, status, headers, config) {
		});
	}
	////listado
	$http.get('/work').
	success(function(data, status, headers, config) 
	{	
		$scope.works = data.works;
	}).
	error(function(data, status, headers, config) {
		//called asynchronously if an error occurs
		//or server returns response with an error status.
	});	
	$scope.onSave = function(e, isValid)
	{
		if(isValid)
		{
			var emit  = {name: $scope.name, information: $scope.information, _id: $scope.uid};
			if($scope.uid>0){
				
				$http.put('/work/'+$scope.uid, {name: $scope.name, information: $scope.information}).
				  success(function(data, status, headers, config) {
				  					  	
				    socket.emit('update work', emit);
				    
				  }).
				  error(function(data, status, headers, config) {
				  });
			}else{
				$http.post('/work', {name: $scope.name, information: $scope.information}).
				  success(function(data, status, headers, config) {
				  	emit._id = data.work[0]._id;
				    socket.emit('new work', emit);
				  }).
				  error(function(data, status, headers, config) {
				  });
			}
			$scope.Reset();
		}		
		e.preventDefault();
		return false;
	}
	console.log("espiri work, con muxa ierda TRABAJOS");
});

watsonApp.controller('homeCtrl', function ($scope, $route, $routeParams) 
{
	//console.log("espiri work, con muxa ierda ", $routeParams);
});
watsonApp.controller('statusCtrl', function ($scope, $route, $routeParams) 
{
	//console.log("espiri work, con muxa ierda ", $routeParams);
});
///////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////breadcrums
watsonApp.directive('atsBreadcrumbs', ['$route', '$routeParams' ,function($route, $routeParams)
{
	
	return {
		restrict: 'A',
		controller: 'breadrumpsCtrl',		
		link: function($scope, $element, $attrs){
			
		},
    	templateUrl: 'views/breadcrumps'
  	};
}]);
watsonApp.controller('breadrumpsCtrl', ['$scope', '$route', '$routeParams',  function ($scope, $route, $routeParams) 
{
	$scope.$on( "$routeChangeSuccess", function(event, next, current) {   	
   		$scope.section = next.$$route.name;
	});
}]);
///////////////////////////////////////////////////////navResponsive
watsonApp.directive('navResponsive', ['matchmedia' ,function($route, $routeParams)
{	
	return {
		restrict: 'A',
		controller: 'navResponsiveCtrl',		
		link: function($scope, $element, $attrs){
			
		}
  	};
}]);
watsonApp.controller('navResponsiveCtrl', ['$scope', 'matchmedia', '$window', '$element', function ($scope, matchmedia, $window, $element) 
{
	var prev = "";
	var act = (matchmedia.isDesktop())?"Desktop":"responsive";
	var w = angular.element($window);
	$scope.onResize = function ()
	{
		act = (matchmedia.isDesktop())?"Desktop":"responsive";
		if(prev!=act){
			$scope.isDesktop = matchmedia.isDesktop(); 
			$scope.collapse = !matchmedia.isDesktop();
			prev = act;
		}		
		
	}
	w.bind('resize', function(){
		$scope.onResize();
	});
	$scope.onResize();
	
	$scope.Toogle = function (e){		
		$scope.collapse = !$scope.collapse;
	}
	
}]);
///////////////////////////////////////////////////////route
watsonApp.config(['$routeProvider',
  function($routeProvider) 
  {  	
    $routeProvider.
      when('/chat/:id?', {
        templateUrl: 'views/sections/chat',
        name: 'chat',
        controller: 'chatCtrl'
      }).
      when('/', {
        templateUrl: 'views/sections/home',
        name:'home',
        controller: 'homeCtrl'
      }).
      when('/trabajos', {
        templateUrl: 'views/sections/works',
        name:'trabajos',
        controller: 'trabajosCtrl'
      }).       
       when('/status', {
        templateUrl: 'views/sections/status',
        name:'status',
        controller: 'statusCtrl'
      }).
      otherwise({
        redirectTo: '/'
      });
  }]);
watsonApp.animation('.collapse', function() {
	return{
		addClass : function(element, className, done) {
			if(className=='in')
			{
				var _h = element.find('.navbar-nav:eq(0)').height()+25;
				TweenMax.fromTo(element, 0.3, { height: 0 }, {height: _h, onComplete: done });
			}else{
				done();
			}
		},
		removeClass : function(element, className, done) {
			if(className=='in')
			{
				element.addClass('in');
				var _h = element.find('.navbar-nav:eq(0)').height();
				TweenMax.fromTo(element, 0.3, { height: _h }, {height: 0, onComplete: function(){
					element.removeClass('in');
					done();
				}});
			}else{
				done();
			}
		}
  	}
});
