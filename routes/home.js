var express = require('express');
var router = express.Router();
/* GET home page. */
router.get('/', function(req, res, next) {	
	console.log(res.locals.name);
  res.render('index', { title: 'Express',  namepassed: 'res.locals.name'});
});

module.exports = router;
