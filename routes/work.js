var express = require('express');

var neo4j = require('node-neo4j');

module.exports = function(vars)
{
	var db = new neo4j('http://localhost:7474');///conectamos 
	return {
		save: function(req, res, index)
		{	
			var cypher = 'CREATE (w:Work { name: "'+req.body.name+'", information: "'+req.body.information+'" }) return  w;';		
			db.cypherQuery(cypher, function(err, result){
		    	if(err)  res.json({"status": false});		    	
			    res.json({"status": true, 'work': result.data});
			});
		},
		get: function(req, res, index)
		{				
			var cypher = (req.params.id!=undefined) ?  'match (w:`Work`) where id(w) = '+req.params.id+' return w;' : 'MATCH (w:`Work`) RETURN w LIMIT 25';			
			db.cypherQuery(cypher, function(err, result)
			{
		    	if(err)  res.json({"status": false});
			    res.json({
			    	"status": true,
			    	works: (req.params.id==undefined)? result.data : result.data[0]
			    });
			});	
		},
		delete: function (req, res, index)
		{
			var cypher = 'START w=node('+req.params.id+') DELETE w;';
			db.cypherQuery(cypher, function(err, result){
		    	if(err)  res.json({"status": false});
			    res.json({"status": true, work: req.params.id});
			});
		},
		update: function (req, res, index)
		{
			var cypher = "start w=node("+req.params.id+") SET w.name = '"+req.body.name+"', w.information='"+req.body.information+"'  return w;"
			db.cypherQuery(cypher, function(err, result){
		    	if(err)  res.json({"status": false});
			    res.json({"status": true, work: req.params.id});
			});
		}

	}
}