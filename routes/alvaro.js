var express = require('express');

module.exports = function(vars){	
	return {
		home: function(req, res, next){
			res.render('index', { title: 'Express',  oleo: vars.test});
		}
	}
}