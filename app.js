var express = require('express')
	,path = require('path')
	,http = require('http')
	,io = require('socket.io')
  ,bodyParser = require('body-parser');

var PORT = process.env.PORT || 3100,
    HOST = process.env.HOST || 'localhost';
var app = express();

/////////////////////
server = http.createServer(app);
io = io.listen(server);
var _socket;
// upon connection, start a periodic task that emits (every 1s) the current timestamp
io.on('connection', function (socket) { 
  ///console.log('iniciado'); 
  socket.emit('new-message', 'con3ectado');    
    socket.on('chat message', function(data)
    {
        io.emit('new-message', data);
    });
    socket.on('new work', function(data) 
    {      
      io.emit('add work', data);
    });
    socket.on('delete work', function(data) 
    {     
      io.emit('clear work', data);
    });
    socket.on('update work', function(data) 
    {     
      io.emit('change work', data);
    });
});
console.log(_socket);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.set('io', io);

app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'bower_components')));

//require('./routes/')(app);
var alvaro = require('./routes/alvaro')({test:'uno', io:io});
var home = require('./routes/home');
var work = require('./routes/work')({io: io});

//app.use(express.json());
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
  extended: true
}));
app.use( bodyParser.json() );       // to support JSON-encoded bodies
/////////////////////////////////////////////////////////////////////////////////////////////////
app.get('/', alvaro.home); //#root scene
app.post('/work', work.save);
app.put('/work/:id?', work.update);
app.get('/work/:id?', work.get);
app.delete('/work/:id?', work.delete);
//app.use('/trabajo', trabajo); //#root trabajo


///////////////////activamos la lectura del servidor
server.listen(PORT, HOST, null, function() {
    console.log('Server listening on port %d in %s mode');
});
module.exports = app;
